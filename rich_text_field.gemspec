# frozen_string_literal: true

require_relative 'lib/rich_text_field/version'

Gem::Specification.new do |spec|
  spec.name        = 'rich_text_field'
  spec.version     = RichTextField::VERSION
  spec.authors     = ['Patrick Pinto']
  spec.email       = ['hello@patricknpinto.com']
  spec.homepage    = 'http://patricknpinto.com'
  spec.summary     = 'Summary of RichTextField.'
  spec.description = 'Description of RichTextField.'
  spec.license     = 'MIT'

  spec.required_ruby_version = '>= 2.7'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/patricknpinto/rich-text-field'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/patricknpinto/rich-text-field'

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  spec.add_dependency 'rails', '> 6.0', '< 8'
  spec.add_dependency 'sanitize', '~> 6.1'

  spec.add_development_dependency 'faker', '~> 2.19'
  spec.add_development_dependency 'rspec-rails', '~> 3.9'
  spec.add_development_dependency 'rubocop', '~> 1.23'
  spec.add_development_dependency 'rubocop-rails', '~> 2.12'
  spec.add_development_dependency 'rubocop-rspec', '~> 2.6'
  spec.add_development_dependency 'simplecov', '~> 0.21'

  spec.add_development_dependency 'pry-byebug', '~> 3.9'
  spec.add_development_dependency 'pry-rails', '~> 0.3'

  spec.metadata = {
    'rubygems_mfa_required' => 'true'
  }
end
