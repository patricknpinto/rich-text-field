# frozen_string_literal: true

require 'sanitize'

SANITIZE_ELEMENTS = %w[b i strong em u ul ol li br p div span pre
                       blockquote code a img h1 h2 h3 h4 h5 h6].freeze
SANITIZE_ATTRIBUTES = {
  'a' => %w[href target rel],
  'span' => %w[class],
  'p' => %w[class],
  'img' => %w[alt]
}.freeze

module RichTextField
  module Base
    extend ActiveSupport::Concern

    # TODO: Didn't figure how to separate this regex in multiple lines
    # rubocop:disable Layout/LineLength
    ANCHOR_TAG_REGEX = %r{(<a\s+[^>]*>.*?</a>)}.freeze
    EMAIL_REGEX = %r{([a-zA-Z0-9.!\#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*)}.freeze
    URL_REGEX = %r{(https?://(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,10}\b([-a-zA-Z0-9()@:%_+.~#?&/=]*))}.freeze
    EMPTY_P_REGEX = %r{<p>( *)</p>}.freeze
    # rubocop:enable Layout/LineLength

    module ClassMethods
      def rich_text_field(*fields)
        fields.each do |field|
          define_method "#{field}=" do |value|
            super(sanitize_text(normalized_rich_text(value)))
          end
        end
      end
    end

    def empty_text?(text)
      text.blank? || Nokogiri::HTML(text).text.strip.blank?
    end

    def normalized_rich_text(text)
      # deal with empty text
      return '' if empty_text?(text)

      # remove whitespaces at beginning and end
      text = text.to_s.strip
      # replace &nbsp; by space
      text = text.gsub('&nbsp;', ' ')
      # replace &amp; by & (because of email with & characters)
      # TODO: this might produce invalid HTML
      text = text.gsub('&amp;', '&')
      # replace <br> by <br />
      text = text.gsub('<br>', '<br />')

      text = replace_empty_lines(text)

      # removes all style= attributes
      text = text.gsub(/ style=".*?"/, '')

      # empty paragraphs with break line at the middle of text
      text = text.gsub(EMPTY_P_REGEX, '<p><br /></p>')

      # find and link urls
      text = replace_urls(text)

      # find and link emails
      replace_emails(text)
    end

    def replace_empty_lines(text)
      # removes empty lines at the beginning of the text
      text = text[0..-14] while text.end_with?('<p><br /></p>')
      # removes empty lines at the end of the text
      text = text[13..] while text.starts_with?('<p><br /></p>')

      text
    end

    def replace_urls(text)
      combined_regex = /
        #{ANCHOR_TAG_REGEX}|
        #{URL_REGEX}
      /x

      text.gsub(combined_regex) do
        # do nothing when already a <a> tag
        if Regexp.last_match(1)
          Regexp.last_match(1)
        elsif Regexp.last_match(2)
          url = Regexp.last_match(2)
          "<a href=\"#{url}\" target=\"_blank\" rel=\"noopener noreferrer\">#{url}</a>"
        end
      end
    end

    def replace_emails(text)
      combined_regex = /
        #{ANCHOR_TAG_REGEX}|
        #{EMAIL_REGEX}
      /x

      text.gsub(combined_regex) do
        # do nothing when already a <a> tag
        if Regexp.last_match(1)
          Regexp.last_match(1)
        elsif Regexp.last_match(2)
          email = Regexp.last_match(2)
          "<a href=\"mailto:#{email}\" target=\"_blank\" rel=\"noopener noreferrer\">#{email}</a>"
        end
      end
    end

    def sanitize_text(text)
      Sanitize.fragment(text, Sanitize::Config.merge(
                                Sanitize::Config::RELAXED,
                                elements: SANITIZE_ELEMENTS,
                                attributes: SANITIZE_ATTRIBUTES
                              ))
    end
  end
end
