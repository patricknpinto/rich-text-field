# frozen_string_literal: true

require 'rich_text_field/version'
require 'rich_text_field/railtie'

require 'rich_text_field/base'

module RichTextField
end
